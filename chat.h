#pragma once

struct Chat_s;

typedef void (*chat_message_handler)(struct Chat_s *chat, const char *line);

struct Chat_s
{
    void *app;
    void *text_view;

    chat_message_handler handler;
    void *user_data;
};

typedef struct Chat_s Chat;

// Window functions
Chat *chat_new(void);
void chat_run(Chat *, int argc, char *argv[]);
void chat_delete(Chat *);

// Writing and reading
void chat_writeln(Chat *, const char *line, size_t length);
