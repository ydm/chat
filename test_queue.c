#include <stddef.h>
#include <stdio.h>
#include "linked_list.h"


void one()
{
    queue *q = queue_new();

    queue_add(q, "one");
    printf("%p %p\n", (void *) q->head, (void *) q->last);

    queue_add(q, "two");
    printf("%p %p\n", (void *) q->head, (void *) q->last);

    queue_add(q, "three");
    printf("%p %p\n", (void *) q->head, (void *) q->last);

    for (queue_node *head = q->head; head != NULL; head = head->next)
    {
        printf("%s\n", head->msg);
    }

    for (int i = 0; i < 10; i++)
    {
        printf("# %d\n", i);
        queue_node *removed = queue_remove(q);
        printf("head=%p, last=%p, removed=%p (%s)\n",
               (void *) q->head, (void *) q->last, (void *) removed,
               (removed ? removed->msg : ""));

        for (queue_node *head = q->head; head != NULL; head = head->next)
        {
            printf("%s\n", head->msg);
        }

        queue_delete_node(removed);
    }

    queue_delete(q);
    q = NULL;
}

void two()
{
    queue *q = queue_new();
    queue_add(q, "one");
    queue_add(q, "two");
    queue_add(q, "three");
    queue_delete(q);
}

int main()
{
    // one();
    two();
}
