#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <arpa/inet.h>
#include <sys/socket.h>

#include <pthread.h>
#include <unistd.h>             // sleep

#include "chat.h"
#include "linked_list.h"

struct context
{
    queue *messages_to_send;
    Chat *chat;
    int sock;
};

static int
create_socket()
{
    int s = socket (AF_INET, SOCK_STREAM, 0);
    if (s < 0)
    {
      perror ("socket");
      exit (EXIT_FAILURE);
    }

    struct sockaddr_in a;
  
    a.sin_family = AF_INET;
    a.sin_port = htons (8888);

    // if (inet_pton (AF_INET, "192.168.1.38", &a.sin_addr) <= 0)
    if (inet_pton (AF_INET, "127.0.0.1", &a.sin_addr) <= 0)
    {
        perror ("inet_pton");
        exit (EXIT_FAILURE);      
    }

    if (connect (s, (const struct sockaddr *) &a, sizeof (a)) < 0)
    {
        perror ("socket");
        exit (EXIT_FAILURE);      
    }

    return s;
}

// TODO: shutdown(sock) AND FREE ALL OTHER RESOURCES TOO!

/*
  char *line = NULL;
  size_t n = 0;
  
  while (1)
    {
      ssize_t read = getline (&line, &n, stdin);

      if (read <= 0)
	break;

      // printf (" line = %s\nn = %zu\nread = %zd\n", line, n, read);
      send (s, line, read-1, 0);
    }
  
  return 0;
}
*/

static void

on_message(Chat *chat, const char *s)
{
    struct context *casted = (struct context *)chat->user_data;
    // queue_add(casted->messages_to_send, s);
    send(casted->sock, s, strlen(s), 0);
}

static void *
worker(void *data)
{
    struct context *ctx = (struct context *) data;
    char buf[512];
    while (1)
    {
        ssize_t n = recv(ctx->sock, buf, 512, 0);
        if (n <= 0)
        {
            break;
        }
        // HANDLE MESSAGE
        buf[n] = '\0';
        printf("msg='%s', len=%zd\n", buf, n);
        chat_writeln(ctx->chat, buf, (size_t) n);
    }
}

int
main(int argc, char *argv[])
{
    Chat *chat = chat_new();
    chat->handler = &on_message;
    struct context ctx = {
        .messages_to_send = queue_new(),
        .chat = chat,
        .sock = create_socket(),
    };
    chat->user_data = &ctx;

    pthread_t t = pthread_create(&t, NULL, &worker, &ctx);

    chat_run(chat, argc, argv);
    chat_delete(chat);

    // chat_writeln(chat, "Kenobi: Hello, there!");
    // chat_writeln(chat, "Grevious: General Kenobi!");
    // chat_set_handler(w, &on_message);

    pthread_join(t, NULL);
    return 0;
}
