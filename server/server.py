#!/usr/bin/env python

import asyncio


class EchoServerProtocol(asyncio.Protocol):

    def __init__(self, manager):
        super().__init__()
        self._manager = manager
        self._transport = None

    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(peername))

        self._transport = transport
        self._manager.add(self)

    def data_received(self, data):
        sock = self._transport.get_extra_info('socket')
        addr, port = sock.getpeername()
        decoded = data.decode('ascii')
        msg = f'{addr}: {decoded}'
        print(msg)
        self._manager.send_to_all(msg)

    def connection_lost(self, exc):
        print('Connection lost')
        self._manager.remove(self)

    def write(self, *a, **kw):
        self._transport.write(*a, **kw)


class Manager:

    def __init__(self):
        self._clients = set()

    def add(self, protocol):
        self._clients.add(protocol)

    def remove(self, protocol):
        # protocol.close()
        self._clients.discard(protocol)

    def send_to_all(self, msg):
        for c in self._clients:
            encoded = msg.encode('ascii')
            print('SENDING: ', encoded)
            c.write(encoded)


async def main():
    loop = asyncio.get_running_loop()

    manager = Manager()
    server = await loop.create_server(lambda: EchoServerProtocol(manager), '', 8888)

    async with server:
        await server.serve_forever()


if __name__ == '__main__':
    asyncio.run(main())
