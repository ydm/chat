CFLAGS=-Wall -Wextra -pedantic -g
GTK_CFLAGS=`pkg-config --cflags gtk+-3.0`
GTK_LIBS=`pkg-config --libs gtk+-3.0`

chat: linked_list.o chat.o main.o
	$(CC) $(CFLAGS) $(GTK_LIBS) -pthread -o chat	\
		linked_list.o chat.o main.o

chat.o: linked_list.h chat.h chat.c
	$(CC) -c $(CFLAGS) $(GTK_CFLAGS) -o chat.o	\
		chat.c

linked_list.o: linked_list.h linked_list.c
	$(CC) -c $(CFLAGS) -o linked_list.o		\
		linked_list.c

main.o: chat.h main.c
	$(CC) -c $(CFLAGS) -o main.o			\
		main.c

test_queue: linked_list.h linked_list.c test_queue.c
	$(CC) $(CFLAGS) -o test_queue			\
		linked_list.h linked_list.c test_queue.c

.PHONY: clean
clean:
	rm -f *.o chat test_queue
