#include <stdlib.h>
#include <gtk/gtk.h>
#include "chat.h"

#define UNUSED __attribute__((unused))

static void
f (GtkWidget *widget, gpointer user_data)
{
    // Get user input.
    const char *text = gtk_entry_get_text (GTK_ENTRY (widget)); // this is the text inside

    // Cast chat structure and invoke handler.
    Chat *chat = (Chat *) user_data;
    chat->handler(chat, text);

    // Emtpy the input field.
    gtk_entry_set_text (GTK_ENTRY (widget), "");
}

// static void
// h (GtkWidget *n, gpointer user_data)
// {
//   GtkWidget *text_view = (GtkWidget *) user_data;
//   GtkTextIter iter;
//   GtkTextBuffer *enter_buf;
//   const char *c = gtk_entry_get_text (GTK_ENTRY (n));

//   enter_buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view));
//   gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (enter_buf), &iter);

//   gtk_text_buffer_insert (GTK_TEXT_BUFFER (enter_buf), &iter, c, -1);
//   gtk_text_buffer_insert (GTK_TEXT_BUFFER (enter_buf), &iter, "\n", -1);
//   gtk_text_buffer_insert (GTK_TEXT_BUFFER (enter_buf), &iter, "---", -1);
//   gtk_text_buffer_insert (GTK_TEXT_BUFFER (enter_buf), &iter, "\n", -1);
// }

static void
activate (GtkApplication *app, gpointer user_data)
{
    Chat *casted = (Chat *) user_data;

    GtkWidget *window, *scroll_win;
    GtkTextBuffer *enter_buf;
    GtkWidget *text_view, *text_enter, *grid;
    // GtkWidget *box, *button;

    window = gtk_application_window_new (app);
    gtk_window_set_title (GTK_WINDOW (window), "Chat Room");
    gtk_window_set_default_size (GTK_WINDOW (window), 300, 300);
    gtk_window_set_resizable (GTK_WINDOW (window), FALSE);
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);

    scroll_win = gtk_scrolled_window_new (NULL, NULL);

    grid = gtk_grid_new ();

    // button = gtk_button_new_with_label ("ENTER");
    // box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 10);
    text_enter = gtk_entry_new ();
    text_view = gtk_text_view_new ();

    // Store a pointer to the text view so we can later use it to
    // implement chat_writeln().
    casted->text_view = text_view;

    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_WORD_CHAR);

    g_object_set_data (G_OBJECT (text_enter), "buf", &enter_buf);
    g_object_set_data (G_OBJECT (text_enter), "area", &text_view);

    // g_signal_connect (text_enter, "activate", G_CALLBACK(h), text_view);
    g_signal_connect (text_enter, "activate", G_CALLBACK(f), casted);

    // g_signal_connect (button, "clicked", G_CALLBACK(h), text_view);
    // g_signal_connect (button, "clicked", G_CALLBACK(f), casted);

    gtk_text_view_set_border_window_size (GTK_TEXT_VIEW (text_view), GTK_TEXT_WINDOW_BOTTOM, 15);

    gtk_widget_set_sensitive (text_view, FALSE);

    gtk_container_add (GTK_CONTAINER (scroll_win), text_view);
    // gtk_container_add (GTK_CONTAINER (box), button);

    gtk_grid_attach (GTK_GRID (grid), text_enter, 0, 3, 3, 1);
    gtk_grid_attach (GTK_GRID (grid), scroll_win, 0, 0, 3, 3);
    // gtk_grid_attach (GTK_GRID (grid), box, 1, 3, 1, 1);

    gtk_widget_set_hexpand (text_enter, TRUE);
    gtk_widget_set_halign (text_enter, GTK_ALIGN_FILL);
    // gtk_widget_set_hexpand (box, TRUE);
    // gtk_widget_set_halign (box, GTK_ALIGN_END);
    gtk_widget_set_vexpand (text_view, TRUE);
    gtk_widget_set_valign (text_view, GTK_ALIGN_FILL);

    gtk_container_add (GTK_CONTAINER (window), grid);

    gtk_widget_show_all (window);
}


Chat *
chat_new(void)
{
  Chat *c = malloc(sizeof(Chat));
  memset(c, 0, sizeof(Chat));

  GtkApplication *app = gtk_application_new ("pxor.chat", G_APPLICATION_FLAGS_NONE);
  c->app = app;

  g_signal_connect (app, "activate", G_CALLBACK (activate), c);
  return c;
}

void
chat_run(Chat *chat, int argc, char *argv[])
{
    /* TODO: status = */
    g_application_run (G_APPLICATION (chat->app), argc, argv);
}

void
chat_delete(Chat *chat UNUSED)
{
    // TODO: DELETE ALL THE GTK SHIT!
    free(chat);
}

void
chat_writeln(Chat *chat, const char *msg, size_t length)
{
    // Get buffer from the stored text_view.
    GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (chat->text_view));

    // Get iterator.
    GtkTextIter iter;
    gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buf), &iter);

    // Insert new text
    gtk_text_buffer_insert (GTK_TEXT_BUFFER (buf), &iter, msg, length);
    gtk_text_buffer_insert (GTK_TEXT_BUFFER (buf), &iter, "\n", -1);
}

void
chat_set_handler(Chat *chat, chat_message_handler handler)
{
    chat->handler = handler;
}
