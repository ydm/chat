#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linked_list.h"

static char *
copy_string(const char *s)
{
    char *copy = malloc((strlen(s) + 1) * sizeof(char));
    strcpy(copy, s);
    return copy;
}

static queue_node *
new_node(const char *s)
{ 
  queue_node *n = malloc(sizeof(queue_node));
  n->msg = copy_string(s);
  n->next = NULL;
  return n;
}

queue *
queue_new()
{
    queue *q = malloc(sizeof(queue));
    q->head = q->last = NULL;
    return q;
}

void
queue_delete (queue *q)
{
    queue_node *iter = q->head;
    while (iter)
    {
        queue_node *next = iter->next;
        queue_delete_node(iter);
        iter = next;
    }
    free(q);
}

void
queue_delete_node(queue_node *n)
{
    if (n)
    {
        free(n->msg);
        free(n);
    }
}

// Add an element to the end of this queue.
void
queue_add (queue *q, const char *s)
{
    if (q->last == NULL)
    {
        q->head = q->last = new_node(s);
    }
    else
    {
        queue_node *node = new_node(s);
        q->last->next = node;
        q->last = node;
    }
}

// Takes one from the beginning of the queue.  User should delete the
// node returned himself using queue_delete_node().
queue_node *queue_remove (queue *q)
{
    if (!q->head)
        return NULL;
    queue_node *ans = q->head;
    q->head = q->head->next;
    if (!q->head)
        q->last = NULL;
    return ans;
}
