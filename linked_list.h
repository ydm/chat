#ifndef QUEUE_H__
#define QUEUE_H__

struct queue_node_s
{
    char *msg;
    struct queue_node_s *next;
};

typedef struct queue_node_s queue_node;

typedef struct
{
    queue_node *head;
    queue_node *last;
} queue;


queue *queue_new();
void queue_delete (queue *);
void queue_delete_node(queue_node *n);
// Add an element to the end of this queue.
void queue_add (queue *q, const char *s);
// Takes one from the beginning of the queue.  User should delete the
// node returned himself using queue_delete_node().
queue_node *queue_remove (queue *q);

#endif // QUEUE_H__
